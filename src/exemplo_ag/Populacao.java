/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplo_ag;

/**
 *
 * @author admin
 */
class Populacao {

    int tamanhoPopulacao = 10;
    Individuo[] individuosPopulacao = new Individuo[10];
    int oMelhor = 0;

    //Inicializa a população com indivíduos aleatórios
    public void iniciaPopulacaoAleatoria(int size) {
        for (int i = 0; i < individuosPopulacao.length; i++) {
            individuosPopulacao[i] = new Individuo();
        }
    }

    //Pega o melhor indivíduo
    public Individuo pegaMelhorIndividuo() {
        int maxFit = Integer.MIN_VALUE;
        int maxFitIndex = 0;
        for (int i = 0; i < individuosPopulacao.length; i++) {
            if (maxFit <= individuosPopulacao[i].fitness) {
                maxFit = individuosPopulacao[i].fitness;
                maxFitIndex = i;
            }
        }
        oMelhor = individuosPopulacao[maxFitIndex].fitness;
        return individuosPopulacao[maxFitIndex];
    }

    //Pega o segundo melhor indivíduo
    public Individuo pegaSegundoMelhor() {
        int maxFit1 = 0;
        int maxFit2 = 0;
        for (int i = 0; i < individuosPopulacao.length; i++) {
            if (individuosPopulacao[i].fitness > individuosPopulacao[maxFit1].fitness) {
                maxFit2 = maxFit1;
                maxFit1 = i;
            } else if (individuosPopulacao[i].fitness > individuosPopulacao[maxFit2].fitness) {
                maxFit2 = i;
            }
        }
        return individuosPopulacao[maxFit2];
    }

    //Pega o pior indivíduo
    public int pegaPior() {
        int minFitVal = Integer.MAX_VALUE;
        int minFitIndex = 0;
        for (int i = 0; i < individuosPopulacao.length; i++) {
            if (minFitVal >= individuosPopulacao[i].fitness) {
                minFitVal = individuosPopulacao[i].fitness;
                minFitIndex = i;
            }
        }
        return minFitIndex;
    }

    //Calcula a função fitness de cada indivíduo
    public void calculaFitnessDosInvividuos() {

        for (int i = 0; i < individuosPopulacao.length; i++) {
            individuosPopulacao[i].calcFitness();
        }
        pegaMelhorIndividuo();
    }

}
