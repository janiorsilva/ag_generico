/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplo_ag;
import java.util.Random;
/**
 *
 * @author admin
 */
public class AG {

    Populacao populacao = new Populacao();
    Individuo individuoSolucao;
    Individuo segundoMelhor;
    int quantidadeGeracoes = 0;

    public static void main(String[] args) {

        Random rn = new Random();
        final int MAX_CICLOS = 1000;//limite máximo de ciclos para rodar o AG - vai parar mesmo que não encontre solução
        AG demo = new AG();

        //Initialize population
        demo.populacao.iniciaPopulacaoAleatoria(10);

        //Calculate fitness of each individual
        demo.populacao.calculaFitnessDosInvividuos();

        System.out.println("Geração: " + demo.quantidadeGeracoes + " Mais adaptado (Melhor função fitness): " + demo.populacao.oMelhor);

        //enquanto a população não encontra o melhor fitness
        while (demo.populacao.oMelhor < 5 && demo.quantidadeGeracoes < MAX_CICLOS) {
            ++demo.quantidadeGeracoes;

            //Selecionar os pais
            demo.selection();

            //Cross over ou CRUZAMENTO
            demo.crossover();

            //Mutação de 7%
            if (rn.nextInt()%7 < 5) {
                demo.mutation();
            }

            //Adicionar o indivíduo com melhor fitness na população (fazer corte elitista)
            demo.addFittestOffspring();

            //Calcular a função de fitness do(s) indivíduo(s)
            demo.populacao.calculaFitnessDosInvividuos();

            System.out.print("Geração: " + demo.quantidadeGeracoes + " Mais adaptado (Melhor função fitness): " + demo.populacao.oMelhor + " Cromossomo do melhor indivíduo: " );
            
            for (int i = 0; i < 5; i++) {
                System.out.print(demo.populacao.pegaMelhorIndividuo().genes[i]);
            }
            System.out.println("");
            
        }

        if(demo.quantidadeGeracoes>=MAX_CICLOS)
            System.out.println("\nRodou: " + MAX_CICLOS + " vezes mas não achou solução." );
        else
            System.out.println("\nSolução encontrada na geração: " + demo.quantidadeGeracoes);

        System.out.println("Valor da Função Fitness do indivíduo solução: "+demo.populacao.pegaMelhorIndividuo().fitness);
        System.out.print("Genes do indivíduo solução: ");
        for (int i = 0; i < 5; i++) {
            System.out.print(demo.populacao.pegaMelhorIndividuo().genes[i]);
        }

        System.out.println("");

    }

    //Selecionar os pais
    void selection() {

        //Selecionar o indivíduo com maior fitness para fazer o cruzamento
        individuoSolucao = populacao.pegaMelhorIndividuo();

        //Selecionar o segundo indivídio com maior fitness para fazer o cruzamento
        segundoMelhor = populacao.pegaSegundoMelhor();
    }

    //Crossover ou cruzamento entre os pais selecionados
    void crossover() {
        Random rn = new Random();

        //Seleciona um ponto de CROSS OVER - ALEATÓRIO
        int crossOverPoint = rn.nextInt(populacao.individuosPopulacao[0].tamCromossomo);

        //Troca os valores entre os pais para obter o novo indivíduo
        for (int i = 0; i < crossOverPoint; i++) {
            int temp = individuoSolucao.genes[i];
            individuoSolucao.genes[i] = segundoMelhor.genes[i];
            segundoMelhor.genes[i] = temp;

        }

    }

    //Mutação
    void mutation() {
        Random rn = new Random();

        //Seleciona um lugar aleatório para fazer a mutação
        int mutationPoint = rn.nextInt(populacao.individuosPopulacao[0].tamCromossomo);

        //Troca os valores do ponto de mutação
        if (individuoSolucao.genes[mutationPoint] == 0) {
            individuoSolucao.genes[mutationPoint] = 1;
        } else {
            individuoSolucao.genes[mutationPoint] = 0;
        }

        mutationPoint = rn.nextInt(populacao.individuosPopulacao[0].tamCromossomo);

        if (segundoMelhor.genes[mutationPoint] == 0) {
            segundoMelhor.genes[mutationPoint] = 1;
        } else {
            segundoMelhor.genes[mutationPoint] = 0;
        }
    }

    //Seleção: elitismo
    Individuo getFittestOffspring() {
        if (individuoSolucao.fitness > segundoMelhor.fitness) {
            return individuoSolucao;
        }
        return segundoMelhor;
    }


    //Substitui o último indivíduo com o melhor
    void addFittestOffspring() {

        //Atualiza o melhor indivíduo
        individuoSolucao.calcFitness();
        segundoMelhor.calcFitness();

        //pega o índice do pior indivíduo
        int leastFittestIndex = populacao.pegaPior();

        //troca o pior indivíduo pelo melhor indivíduo
        populacao.individuosPopulacao[leastFittestIndex] = getFittestOffspring();
    }

}