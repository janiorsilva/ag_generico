/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplo_ag;
import java.util.Random;
/**
 *
 * @author admin
 */
class Individuo implements Cloneable{

    int fitness = 0;
    int[] genes = new int[5];
    int tamCromossomo = 5;

    public Individuo() {
        Random rn = new Random();

        //Faz os genes aleatórios para cada indivíduo
        for (int i = 0; i < genes.length; i++) {
            genes[i] = Math.abs(rn.nextInt() % 2);
        }

        fitness = 0;
    }

    //Calcula a função de FITNESS: Se o gene é 1, soma 1. Senão soma 0. O maior valor da soma é o melhor fitness
    public void calcFitness() {

        fitness = 0;
        for (int i = 0; i < 5; i++) {
            if (genes[i] == 1) {
                ++fitness;
            }
        }
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        Individuo individual = (Individuo)super.clone();
        individual.genes = new int[5];
        for(int i = 0; i < individual.genes.length; i++){
            individual.genes[i] = this.genes[i];
        }
        return individual;
    }
}
